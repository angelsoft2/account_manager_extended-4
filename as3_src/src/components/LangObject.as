package components 
{
    public class LangObject extends Object
    {
        public var window_title_l10n: *;
        public var cancel_l10n: *;
        public var save_l10n: *;
        public var server_l10n: *;
        public var password_l10n: *; 
        public var show_password_l10n: *; 
        public var nick_l10n: * ;

        public var submit_l10n: *;
        public var edit_l10n: *;
        public var delete_l10n: *;
        public var add_l10n: *;
        public var auto_enter_l10n: *;
    }
}
